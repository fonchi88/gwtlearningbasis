﻿Feature: SpecFlowFeature1
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag
Scenario: Add two numbers
	Given I have entered 50 into the calculator
	And I have also entered 70 into the calculator
	When I press add
	Then the result should be 120 on the screen

@BrowserSelenium
Scenario: Search for dogs on Google
Given The Google homepage is displayed
When The user search for "dogs"
Then Search results for "dogs" are displayed