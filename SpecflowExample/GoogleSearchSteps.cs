﻿using System;
using TechTalk.SpecFlow;
using Example;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpecflowExample
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        //private GoogleSearch gs;
        //public string url = "http://www.google.co.in";

        private GoogleHomePage ghp;

        [Given(@"The Google homepage is displayed")]
        public void GivenTheGoogleHomepageIsDisplayed()
        {
            //Driver.StartBrowser();
            //gs = new GoogleSearch(Driver.Browser);
            //gs.url = url;
            //gs.navigateTo();
            Driver.StartBrowser(BrowserTypes.InternetExplorer);
            ghp = new GoogleHomePage(Driver.Browser);
            ghp.Open();

        }
        
        [When(@"The user search for ""(.*)""")]
        public void WhenTheUserSearchFor(string search)
        {

            //gs.searchKeyword = search;
            //gs.googleSearch();
            ghp.googleSearch(search);
        }
        
        [Then(@"Search results for ""(.*)"" are displayed")]
        public void ThenSearchResultsForAreDisplayed(string search)
        {

            //Assert.IsTrue(gs.searchResultCount() > 0);
            Assert.IsTrue(ghp.searchResultCount(search) > 0);
        }
    }
}
