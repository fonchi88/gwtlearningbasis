﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Example
{
    public abstract class BasePage
    {
        protected IWebDriver Driver;
        protected WebDriverWait DriverWait;
        public BasePage(IWebDriver driver){
            Driver = driver;
            DriverWait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
        }

        public virtual string Url {
            get {
                return string.Empty;
            }
        }

        public virtual void Open(string part = "") {
            if (string.IsNullOrEmpty(Url)) {
                throw new ArgumentException("The main Url cannot be null or empty");
            }
            Driver.Navigate().GoToUrl(string.Concat(Url,part));
        }
    }
}
