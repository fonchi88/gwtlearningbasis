﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Example
{
    public enum BrowserTypes
    {
        Firefox,
        InternetExplorer,
        Chrome,
        NotSet
    }
}
