﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;

namespace Example
{
    public static class Driver
    {
        private static IWebDriver browser;
        public static IWebDriver Browser
        {
            set
            {
                browser = value;
            }
            get
            {
                return browser;
            }
        }

        public static void StartBrowser(BrowserTypes browserType = BrowserTypes.Chrome) {

            switch (browserType)
            {
                case BrowserTypes.Chrome:
                    Browser = new ChromeDriver();
                    break;
                case BrowserTypes.Firefox:
                    Browser = new FirefoxDriver();
                    break;
                case BrowserTypes.InternetExplorer:
                    Browser = new InternetExplorerDriver();
                    break;
                default:
                    break;
            }
            
        }

        public static void closePage()
        {
            Browser.Close();
            Browser.Dispose();
        }

    }
}
