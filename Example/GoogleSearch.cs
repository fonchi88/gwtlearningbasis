﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Linq;


namespace Example
{
    public class GoogleSearch
    {
        public string searchKeyword { get; set; }
        public string url { get; set; }

        protected IWebDriver Driver;

        public GoogleSearch(IWebDriver driver) {
            Driver = driver;
        }
        public void navigateTo() {
            Driver.Url = url;
        }
        public void googleSearch() {         
            var inputsearch = Driver.FindElement(By.Name("q"));
            inputsearch.SendKeys(searchKeyword);
            inputsearch.SendKeys(Keys.Enter);
            //searchResultCount();
        }

        public int searchResultCount() {
            var results = Driver.FindElements(By.XPath("//div[@class='g']//h3[@class='LC20lb'][contains(text(),'"+ searchKeyword + "')]")).Count();
            return results;
        }

        public void closePage() {
            Driver.Close();
            Driver.Dispose();
        }

    }
}
