﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Linq;

namespace Example
{
    public partial class GoogleHomePage:BasePage
    {
        //private static IWebDriver driver;
        
        public GoogleHomePage(IWebDriver driver):base(driver) {
        
        }

        private IWebElement inputSrch;

        private string resultLink(string keyword) {
            return "//h3[contains(@class,'LC20lb')]/span[contains(translate(.,'ABCDEFGHIJKLMNOPURSTUWXYZ','abcdefghijklmnopurstuwxyz'),'" + keyword + "')]";
        }

        public override string Url {
            get {
                return "http://www.google.co.in";
            }
        }
        public IWebElement InputSearch {
            get { 
                return Driver.FindElement(By.Name("q"));
            }
        }

        public  void googleSearch(string searchKeyword)
        {
            InputSearch.SendKeys(searchKeyword);
            InputSearch.SendKeys(Keys.Enter);
        }

        public int searchResultCount(string searchKeyword)
        {
            var header = By.XPath(resultLink(searchKeyword));
            DriverWait.Until(drv => drv.FindElement(header));
            var results = Driver.FindElements(header).Count();
            return results;
        }

        public void closePage()
        {
            Driver.Close();
            Driver.Dispose();
        }
    }
}
